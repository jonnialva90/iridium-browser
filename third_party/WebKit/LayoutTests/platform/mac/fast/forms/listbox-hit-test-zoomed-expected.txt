layer at (0,0) size 800x600
  LayoutView at (0,0) size 800x600
layer at (0,0) size 800x600
  LayoutBlockFlow {HTML} at (0,0) size 800x600
    LayoutBlockFlow {BODY} at (9.59,9.59) size 780.81x580.81
      LayoutBlockFlow (anonymous) at (0,0) size 780.81x162
        LayoutText {#text} at (0,0) size 0x0
      LayoutBlockFlow {DIV} at (0,162) size 780.81x66
        LayoutText {#text} at (0,0) size 62x22
          text run at (0,0) width 62: "Results:"
        LayoutBR {BR} at (61,17) size 1x0
        LayoutText {#text} at (0,22) size 105x22
          text run at (0,22) width 105: "Test 1 Passed"
        LayoutBR {BR} at (104,39) size 1x0
        LayoutText {#text} at (0,44) size 105x22
          text run at (0,44) width 105: "Test 2 Passed"
layer at (10,10) size 125x162 clip at (22,22) size 90x138 scrollHeight 316
  LayoutListBox {SELECT} at (0,0) size 125.16x162 [bgcolor=#FFFFFF] [border: (12px solid #000000)]
    LayoutBlockFlow {OPTION} at (24,24) size 66.16x17.19
      LayoutText {#text} at (2,0) size 23x16
        text run at (2,0) width 23: "one"
    LayoutBlockFlow {OPTION} at (24,41.19) size 66.16x17.19
      LayoutText {#text} at (2,0) size 23x16
        text run at (2,0) width 23: "two"
    LayoutBlockFlow {OPTION} at (24,58.38) size 66.16x17.19
      LayoutText {#text} at (2,0) size 31x16
        text run at (2,0) width 31: "three"
    LayoutBlockFlow {OPTION} at (24,75.56) size 66.16x17.19
      LayoutText {#text} at (2,0) size 24x16
        text run at (2,0) width 24: "four"
    LayoutBlockFlow {OPTION} at (24,92.75) size 66.16x17.19
      LayoutText {#text} at (2,0) size 22x16
        text run at (2,0) width 22: "five"
    LayoutBlockFlow {OPTION} at (24,109.94) size 66.16x17.19
      LayoutText {#text} at (2,0) size 18x16
        text run at (2,0) width 18: "six"
    LayoutBlockFlow {OPTION} at (24,127.13) size 66.16x17.19 [color=#FFFFFF] [bgcolor=#0069D9]
      LayoutText {#text} at (2,0) size 36x16
        text run at (2,0) width 36: "seven"
    LayoutBlockFlow {OPTION} at (24,144.31) size 66.16x17.19
      LayoutText {#text} at (2,0) size 30x16
        text run at (2,0) width 30: "eight"
    LayoutBlockFlow {OPTION} at (24,161.50) size 66.16x17.19
      LayoutText {#text} at (2,0) size 26x16
        text run at (2,0) width 26: "nine"
    LayoutBlockFlow {OPTION} at (24,178.69) size 66.16x17.19
      LayoutText {#text} at (2,0) size 20x16
        text run at (2,0) width 20: "ten"
    LayoutBlockFlow {OPTION} at (24,195.88) size 66.16x17.19
      LayoutText {#text} at (2,0) size 40x16
        text run at (2,0) width 40: "eleven"
    LayoutBlockFlow {OPTION} at (24,213.06) size 66.16x17.19
      LayoutText {#text} at (2,0) size 39x16
        text run at (2,0) width 39: "twelve"
    LayoutBlockFlow {OPTION} at (24,230.25) size 66.16x17.19
      LayoutText {#text} at (2,0) size 46x16
        text run at (2,0) width 46: "thirteen"
    LayoutBlockFlow {OPTION} at (24,247.44) size 66.16x17.19
      LayoutText {#text} at (2,0) size 51x16
        text run at (2,0) width 51: "fourteen"
    LayoutBlockFlow {OPTION} at (24,264.63) size 66.16x17.19
      LayoutText {#text} at (2,0) size 38x16
        text run at (2,0) width 38: "fifteen"
    LayoutBlockFlow {OPTION} at (24,281.81) size 66.16x17.19
      LayoutText {#text} at (2,0) size 44x16
        text run at (2,0) width 44: "sixteen"
    LayoutBlockFlow {OPTION} at (24,299) size 66.16x17.19
      LayoutText {#text} at (2,0) size 62x16
        text run at (2,0) width 62: "seventeen"
