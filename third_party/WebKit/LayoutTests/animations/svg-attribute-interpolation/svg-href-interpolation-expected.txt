CONSOLE WARNING: SVG's SMIL animations (<animate>, <set>, etc.) are deprecated and will be removed. Please use CSS animations or Web animations instead.

SVG SMIL:
PASS: href from [#A] to [#B] was [#A] at 0
PASS: href from [#A] to [#B] was [#A] at 0.2
PASS: href from [#A] to [#B] was [#B] at 0.6
PASS: href from [#A] to [#B] was [#B] at 1

Web Animations API:
PASS: href from [#A] to [#B] was [#A] at -0.4
PASS: href from [#A] to [#B] was [#A] at 0
PASS: href from [#A] to [#B] was [#A] at 0.2
PASS: href from [#A] to [#B] was [#B] at 0.6
PASS: href from [#A] to [#B] was [#B] at 1
PASS: href from [#A] to [#B] was [#B] at 1.4

