// Copyright 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "ios/chrome/browser/chrome_url_constants.h"

const char kChromeUINewTabURL[] = "chrome://newtab/";

const char kChromeUIExternalFileHost[] = "external-file";
const char kChromeUINetExportHost[] = "net-export";
const char kChromeUIOmahaHost[] = "omaha";
const char kChromeUISyncInternalsHost[] = "sync-internals";
