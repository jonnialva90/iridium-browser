// Copyright 2015 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef UI_GFX_COLOR_PALETTE_H_
#define UI_GFX_COLOR_PALETTE_H_

#include "third_party/skia/include/core/SkColor.h"

namespace gfx {

const SkColor kAmber = SkColorSetRGB(0xFF, 0xC1, 0x07);
const SkColor kChromeIconGrey = SkColorSetRGB(0x5A, 0x5A, 0x5A);
const SkColor kErrorRed = SkColorSetRGB(0xC5, 0x39, 0x29);
const SkColor kGoogleBlue = SkColorSetRGB(0x42, 0x85, 0xF4);

}  // namespace gfx

#endif  // UI_GFX_COLOR_PALETTE_H_
